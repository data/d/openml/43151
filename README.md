# OpenML dataset: Asia_dataset

https://www.openml.org/d/43151

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

** Dataset description**
A synthetic dataset from Lauritzen and Spiegelhalter (1988) about lung diseases (tuberculosis, lung cancer or bronchitis) and visits to Asia.

**Format of the dataset**

A data frame with 5000 rows and 8 binary variables:

    D (dyspnoea), binary 1/0 corresponding to "yes" and "no"

    T (tuberculosis), binary 1/0 corresponding to "yes" and "no"

    L (lung cancer), binary 1/0 corresponding to "yes" and "no"

    B (bronchitis), binary 1/0 corresponding to "yes" and "no"

    A (visit to Asia), binary 1/0 corresponding to "yes" and "no"

    S (smoking), binary 1/0 corresponding to "yes" and "no"

    X (chest X-ray), binary 1/0 corresponding to "yes" and "no"

    E (tuberculosis versus lung cancer/bronchitis), binary 1/0 corresponding to "yes" and "no"

**Source**
https://www.bnlearn.com/bnrepository/

**References**
Lauritzen S, Spiegelhalter D (1988). 'Local Computation with Probabilities on Graphical Structures and their Application to Expert Systems (with discussion)'. Journal of the Royal Statistical Society: Series B 50, 157-224.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43151) of an [OpenML dataset](https://www.openml.org/d/43151). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43151/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43151/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43151/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

